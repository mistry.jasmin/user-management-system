<?php

namespace App\Controller;

use App\Entity\Group;
use App\Form\GroupType;
use App\Repository\GroupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class GroupController extends AbstractController
{
    /**
     * @Route("/group", name="group_list")
     */
    public function group(): Response
    {
        $groups = $this->get(GroupRepository::class)->findAll();

        return $this->render('group/list.html.twig', [
            'groups' => $groups,
        ]);
    }

    /**
     * @Route("/group/new", name="group_new")
     */
    public function add(Request $request): Response
    {
        $group = new Group();
        $form = $this->createForm(GroupType::class, $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $group = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($group);
            $entityManager->flush();
            $this->addFlash(
                'success',
                'Group was saved!'
            );

            return $this->redirectToRoute('group_list');
        }

        return $this->render('group/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("group/delete/{id}", name="group_delete")
     */
    public function delete(int $id, Request $request): Response
    {
        $group = $this->get(GroupRepository::class)->find($id);
        if (!$group) {
            throw $this->createNotFoundException(
                'No Group found for id '.$id
            );
        }
        if (0 !== $group->getUsers()->count()) {
            throw $this->createNotFoundException(
                'Group has atleast one user '
            );
        }

        $form = $this->createFormBuilder($group)
            ->add('delete', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($group);
            $entityManager->flush();
            $this->addFlash(
                'success',
                'Group was deleted!'
            );

            return $this->redirectToRoute('group_list');
        }

        return $this->render('group/delete.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public static function getSubscribedServices(): array
    {
        return array_merge(parent::getSubscribedServices(), [
            GroupRepository::class,
        ]);
    }
}
