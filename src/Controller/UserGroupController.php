<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\GroupRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserGroupController extends AbstractController
{
    /**
     * @Route("/add-group/{id}", name="users_to_group")
     */
    public function addGroup(int $id, Request $request)
    {
        $group = $this->get(GroupRepository::class)->find($id);
        if (!$group) {
            throw $this->createNotFoundException(
                'No Group found for id '.$id
            );
        }

        $form = $this->createFormBuilder($group)
            ->add('users', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (UserRepository $er) use ($id) {
                    return $er->createQueryBuilder('u')
                        ->leftJoin('u.groups', 'g')
                        ->where('g.id IS NULL OR g.id != :group_id')
                        ->setParameter('group_id', $id)
                        ->orderBy('u.name', 'ASC');
                },
                'choice_label' => 'name',
                'multiple' => true,
            ])
            ->add('add', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $group = $form->getData();

            /** @var User $user */
            foreach ($group->getUsers() as $user) {
                $user->addGroup($group);
            }
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(
                'success',
                'Users were added!'
            );

            return $this->redirectToRoute('group_list');
        }

        return $this->render('user/add_group.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete-group/{id}", name="delete_users_to_group")
     */
    public function deleteGroup(int $id, Request $request)
    {
        $group = $this->get(GroupRepository::class)->find($id);
        if (!$group) {
            throw $this->createNotFoundException(
                'No Group found for id '.$id
            );
        }

        $form = $this->createFormBuilder()
            ->add('users', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (UserRepository $er) use ($id) {
                    return $er->createQueryBuilder('u')
                        ->leftJoin('u.groups', 'g')
                        ->where('g.id = :group_id')
                        ->setParameter('group_id', $id)
                        ->orderBy('u.name', 'ASC');
                },
                'choice_label' => 'name',
                'multiple' => true,
            ])
            ->add('delete', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $users = $form->getData()['users'];

            /** @var User $user */
            foreach ($users as $user) {
                $user->removeGroup($group);
            }

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(
                'success',
                'Users were deleted!'
            );

            return $this->redirectToRoute('group_list');
        }

        return $this->render('user/delete_group.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public static function getSubscribedServices(): array
    {
        return array_merge(parent::getSubscribedServices(), [
            UserRepository::class,
            GroupRepository::class,
        ]);
    }
}
